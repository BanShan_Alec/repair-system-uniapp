import Vue from 'vue'
import Vuex from 'vuex'
import reqApi from '@/utils/request.js'

Vue.use(Vuex)

const DEFAULT_AVATAR_URL =
  'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-9f529be0-cd8b-49cc-b792-4803755e0677/bae452e6-9a21-416f-b746-20fc5ee8d0e6.png'

const store = new Vuex.Store({
  state: {
    userInfo: {},
    buildingList: [],
  },
  mutations: {
    setUserInfo(state, data) {
      state.userInfo = data
    },
    setBuildingList(state, data) {
      state.buildingList = data
    },
  },
  actions: {
    // lazy loading openid
    getUserOpenId: async function ({ commit, state }) {
      return await new Promise((resolve, reject) => {
        if (state.openid) {
          resolve(state.openid)
        } else {
          uni.login({
            success: (data) => {
              commit('login')
              setTimeout(function () {
                //模拟异步请求服务器获取 openid
                const openid = '123456789'
                console.log('uni.request mock openid[' + openid + ']')
                commit('setOpenid', openid)
                resolve(openid)
              }, 1000)
            },
            fail: (err) => {
              console.log(
                'uni.login 接口调用失败，将无法正常使用开放接口等服务',
                err,
              )
              reject(err)
            },
          })
        }
      })
    },
    async getUserInfo({ commit, state }) {
      const res = await reqApi.user('getUserInfo')
      if (res.code) {
        return reqApi.catchE(res.code, '获取用户失败：' + res.msg)
      }
      // 初始化数据
      // babel不支持 ??=
      res.userInfo.avatar || (res.userInfo.avatar = DEFAULT_AVATAR_URL)
      // res.userInfo.nickname ??= ''
      // res.userInfo.mobile ??= ''
      // res.userInfo.gender ??= 0
      // res.userInfo.building_id ??= 0
      // res.userInfo.birth_date ??= ''

      commit('setUserInfo', res.userInfo)
      // state.userInfo = res.userInfo
    },
    async getBuildingList({ commit }) {
      const res = await reqApi.building('getBuildings')
      if (res.code) {
        return reqApi.catchE(res.code, '获取失败：' + res.msg)
      }
      commit('setBuildingList', res.data.data)
      // state.buildingList = res.data.data.map((item) => ({
      //   value: item.build_id,
      //   text: item.build_name,
      // }))
    },
  },
})

export default store
