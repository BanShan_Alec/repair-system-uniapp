import Vue from 'vue'
import App from './App'
import store from './store'
// css 云打包不支持
// import './static/css/global.css'
// import './static/css/animate.min.css'
// utils
import * as filters from './utils/filters.js'
import request from './utils/request.js'
import uniCrazyRouter from './router/index.js' // 引入路由

// 注册路由
Vue.use(uniCrazyRouter)

// 注册全局过滤器
Object.keys(filters).forEach(key => {
	Vue.filter(key, filters[key])
})
// 注册全局函数reqApi
Vue.prototype.$reqApi = request
// 注册vuex
Vue.prototype.$store = store

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
