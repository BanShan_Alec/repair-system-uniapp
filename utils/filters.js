const dateFormat = function(timestamp) {
  if (typeof timestamp !== 'number') timestamp = Number(timestamp)
	// 精准到秒需要 *1000, 毫秒不用
  const date = new Date(timestamp * 1000)
  // padStart方法，补全字符，不够长'0'来补
  // getMonth是从0开始算
  const yyyy = date.getFullYear()
  const mo = (date.getMonth() + 1 + '').padStart(2, '0')
  const dd = (date.getDate() + '').padStart(2, '0')
  const hh = (date.getHours() + '').padStart(2, '0')
  const mm = (date.getMinutes() + '').padStart(2, '0')
  const ss = (date.getSeconds() + '').padStart(2, '0')
  return `${yyyy}-${mo}-${dd} ${hh}:${mm}:${ss}`
}

export { dateFormat }
