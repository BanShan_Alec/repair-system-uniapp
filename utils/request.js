const getCloudFnApi = (apiName, action, params, { needLoading }) => {
  needLoading &&
    uni.showLoading({
      title: '请求中',
    })
  return new Promise((resolve) => {
    uniCloud.callFunction({
      name: apiName,
      data: {
        action,
        params,
      },
      success: (res) => {
        resolve(res.result)
      },
      fail: (res) => {
        resolve(res)
      },
      complete: (res) => {
        needLoading && uni.hideLoading()
      },
    })
  })
}

function user(action, params = {}, config = { needLoading: false }) {
  return getCloudFnApi('user-center', action, params, config)
}

function building(action, params = {}, config = { needLoading: false }) {
  return getCloudFnApi('building-center', action, params, config)
}

function order(action, params = {}, config = { needLoading: false }) {
  return getCloudFnApi('order-center', action, params, config)
}

function catchE(code, msg) {
  switch (code) {
    case 30201:
    case 30202:
    case 30203:
    case 30204: {
      uni.showToast({
        title: msg,
        icon: 'none',
        duration: 1200,
        complete() {
          setTimeout(function () {
            uni.redirectTo({
              url: '/pages/login/login',
            })
          }, 500)
        },
      })
      break
    }
    default: {
      uni.showToast({
        title: msg,
        icon: 'none',
      })
    }
  }
}

function catchS(msg) {
  uni.showToast({
    title: msg,
    icon: 'success',
  })
}

export default {
  user,
  building,
  order,
  catchE,
  catchS,
}
