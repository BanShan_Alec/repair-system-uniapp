// 引入公共模块uniID
const uniID = require('uni-id')
// 获取数据库引用
const db = uniCloud.database()
// 获取数据表（集合）引用
const collection = db.collection('order')
// 获取当前时间（精准到秒）
const time = Math.round(new Date() / 1000)
//检查order数据
function checkOrder(orderForm) {
	for (let key in orderForm) {
		if (!orderForm[key]) return false
	}
	return true
}
//处理order_state数据
function getStateArray(state) {
	// 判断是否为number
	if (state === "" || state == null) return []
	if (isNaN(state)) return []

	if (state < 0) return [state]
	return [0, 1, 2, 3, 4, 5]
}

exports.main = async (event, context) => {
	// 封装成功消息体
	let res = {
		code: 0,
		msg: '请求成功'
	}
	const params = event.params
	const release = []
	if (release.indexOf(event.action) === -1) {
		const token_check = await uniID.checkToken(event.uniIdToken, {
			needUserInfo: true,
			needPermission: true
		})
		if (!token_check.code) {
			params.uid = token_check.uid
		} else {
			return token_check
		}
	}

	switch (event.action) {
		case 'getOrderbyStuId': {
			const {
				uid,
				state
			} = params
			res.data = await collection.aggregate()
				.match({
					stu_id: uid,
					state
				})
				.sort({
					update_time: -1
				})
				.lookup({
					from: 'order-state',
					localField: 'state',
					foreignField: 'order_state_id',
					as: 'order_state',
				})
				.lookup({
					from: 'building',
					localField: 'building_id',
					foreignField: 'build_id',
					as: 'building_info',
				})
				.end()
			break
		}
		case 'getOrderbyOderId': {
			const {
				uid,
				id
			} = params
			res.data = await collection.aggregate()
				.match({
					_id: id,
					stu_id: uid,
				})
				.lookup({
					from: 'order-state',
					localField: 'state',
					foreignField: 'order_state_id',
					as: 'order_state',
				})
				.lookup({
					from: 'building',
					localField: 'building_id',
					foreignField: 'build_id',
					as: 'building_info',
				})
				.end()
			break
		}
		case 'newOrder': {
			const {
				uid,
				orderForm
			} = params
			// nosql操作无法回滚, 需校验数据
			if (!checkOrder(orderForm)) {
				return {
					code: 40101,
					msg: '非法数据'
				}
			}
			res.data = await collection.add({
				stu_id: uid,
				create_time: time,
				update_time: time,
				state: 1,
				name: orderForm.name,
				mobile: orderForm.mobile,
				building_id: orderForm.building_id,
				location: orderForm.location,
				describe: orderForm.describe,
				pic_urls: orderForm.pic_urls
			})

			break
		}
		case 'getOrderStatebyState': {
			const {
				uid,
				state
			} = params
			const stateArray = getStateArray(state)

			res.data = await db.collection('order-state')
				.where({
					order_state_id: db.command.in(stateArray)
				})
				.field({
					'_id': false,
					'order_state_id': false,
					'order_state_icon': false
				})
				.orderBy("order_state_id", "asc")
				.get()

			break
		}
		case 'cancelOrder': {
			const {
				uid,
				id,
				state
			} = params
			const stateAllow = [0,1,2]
			if(stateAllow.indexOf(state) === -1){
				return {
					code: 40102,
					msg: '该订单状态不允许撤销'
				}
			}
			// 用户撤销订单
			res.date = await collection.doc(id).update({
				state: -2,
				update_time: time
			})
			
			break
		}
		default: {
			res = {
				code: 401,
				msg: '非法请求'
			}
		}
	}
	//返回数据给客户端
	return res
}
