// 引入公共模块uniID
const uniID = require('uni-id')
// 获取数据库引用
const db = uniCloud.database()
// 获取数据表（集合）引用
const collection = db.collection('building')

exports.main = async (event, context) => {
	// 封装成功消息体
	let res = {
		code: 0,
		msg: '请求成功'
	}
	// 无需检查token
	// const params = event.params
	// const release = []
	// if (release.indexOf(event.action) === -1) {
	// 	const token_check = await uniID.checkToken(event.uniIdToken, {
	// 		needUserInfo: true,
	// 		needPermission: true
	// 	})
	// 	if (!token_check.code) {
	// 		params.uid = token_check.uid
	// 	} else {
	// 		return token_check
	// 	}
	// }

	switch (event.action) {
		case 'getBuildings': {
			res.data = await collection.field({
				'_id': false
			}).orderBy('build_id', 'asc').get()
			break
		}
		default: {
			res = {
				code: 401,
				msg: '非法请求',
				data: {}
			}
		}
	}
	//返回数据给客户端
	return res
}
