const uniID = require('uni-id')

exports.main = async (event, context) => {
	let res = {}
	const params = event.params
	const release = ['register', 'login', 'logout']
	if (release.indexOf(event.action) === -1) {
		const token_check = await uniID.checkToken(event.uniIdToken, {
			needUserInfo: true,
			needPermission: true
		})
		if (!token_check.code) {
			params.uid = token_check.uid
		} else {
			return token_check
		}
	}

	switch (event.action) {
		case 'login': {
			const {
				username,
				password
			} = params
			res = await uniID.login({
				username,
				password,
				queryField: ['username']
			})
			break
		}
		case 'register': {
			const {
				username,
				password
			} = params
			res = await uniID.register({
				username,
				password
			})
			break
		}
		case 'logout': {
			res = await uniID.logout(event.uniIdToken)
			break
		}

		case 'changePassword': {
			const {
				uid,
				oldPassword,
				newPassword
			} = params
			res = await uniID.updatePwd({
				uid,
				oldPassword,
				newPassword
			})
			break
		}
		case 'changeAvatar': {
			const {
				uid,
				avatar
			} = params
			res = await uniID.updateUser({
				uid,
				avatar
			})
			break
		}
		case 'changeUserInfo': {
			const {
				uid,
				userInfo
			} = params
			res = await uniID.updateUser({
				uid,
				avatar: userInfo.avatar,
				nickname: userInfo.nickname,
				mobile: userInfo.mobile,
				gender: userInfo.gender,
				building_id: userInfo.building_id,
				birth_date: userInfo.birth_date,
			})
			break
		}

		case 'getUserInfo': {
			const {
				uid
			} = params
			const field = [
				'avatar',
				'nickname',
				'mobile',
				'gender',
				'building_id',
				'birth_date'
			]
			res = await uniID.getUserInfo({
				uid,
				field
			})
			break
		}
		case 'getUserSimple': {
			const {
				uid
			} = params
			const field = [
				'avatar',
				'nickname'
			]
			res = await uniID.getUserInfo({
				uid,
				field
			})
			break
		}

		default: {
			res = {
				code: 401,
				msg: '非法请求'
			}
		}
	}

	return res
}
