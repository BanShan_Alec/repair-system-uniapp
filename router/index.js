import Vue from 'vue'
import uniCrazyRouter from "uni-crazy-router";
Vue.use(uniCrazyRouter)

uniCrazyRouter.beforeEach(async (to, from, next) => {
	// 允许未登录，访问 home,login,register
	const release = ['/', 'pages/home/home', 'pages/login/login', 'pages/register/register']
	if (release.indexOf(to.url) === -1) {
		// 获取token
		const tokenStr = uni.getStorageSync('uni_id_token')
		// console.log(tokenStr)
		// 没有登录，强制跳转
		if (!tokenStr) {
			uniCrazyRouter.afterNotNext(() => {
				// 拦截路由，并且跳转去其他页面
				uni.showToast({
					title: '请先登录',
					icon: 'none',
					duration: 1200,
					complete() {
						setTimeout(function() {
							uni.redirectTo({
								url: '/pages/login/login'
							})
						}, 800)
					}
				})
				return
			})
			return // 拦截路由，不执行next
		}
	}
	//放行
	next()
})

uniCrazyRouter.afterEach((to, from) => {
	// 逻辑代码
})

uniCrazyRouter['on' + 'Error']((to, from) => {
	// 逻辑代码
})
